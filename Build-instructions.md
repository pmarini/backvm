* While developing, the package can be installed from the local folder. Move to `backvm`:

	```{shell}
	virtualenv /tmp/venv
	/tmp/venv/bin/pip --no-cache-dir install .
	```

* Once everything is tested and ready for update
    * Update the version in `setup.py` and in `backvm/__init__.py`
    * Fill the CHANGELOG
    * Commit the changes with a meaningful commit message
    * Create a release

    	```{shell}
    	git tag -a vx.y.z -m "Release x.y.z"
    	git push origin vx.y.z
    	```

    * If you need to delete the release
    
        ```{shell}
        git push --delete origin vx.y.z`
        ```
    * Download the release just created, expand it, and move to the folder where the setup.py is located

    * To make sure that you have always the latest versions of the modules needed for packaging you can build a virtual environment for each new version

    	```{shell}
    	virtualenv /opt/pyvenv/packaging
    	/opt/pyvenv/packaging/bin/pip --no-cache-dir install twine
    	```

    * Create the wheel

    	```{shell}
    	/opt/pyvenv/packaging/bin/python setup.py bdist_wheel --universal
    	```

    * Upload to pypi - Change the version accordingly

    	```{shell}
    	/opt/pyvenv/packaging/bin/twine  upload dist/backvm-x-y.z-py2.py3-none-any.whl
    	```

    * Check that you can install the new version

       	```{shell}
       	cd /tmp
       
       	rm -rf venv
       
       	virtualenv venv
       
       	/tmp/venv/bin/pip --no-cache-dir install backvm
       
       	/tmp/venv/bin/python -c 'import backvm; print(backvm.__version__)'
       	```
